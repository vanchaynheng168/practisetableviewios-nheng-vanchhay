//
//  ViewController.swift
//  PractiseTableView
//
//  Created by BTB_011 on 11/17/20.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var table: UITableView!
    var modelName: [[String]] = [
        ["vanchay", "Nita", "Linghor","Boung"],
        ["vanchay 1", "Nita 1", "Linghor 1","Boung 1"],
        ["vanchay 2", "Nita 2", "Linghor 2","Boung 2"],
    ]
    override func viewDidLoad() {
        super.viewDidLoad()
        table.delegate = self
        table.dataSource = self
//        table.register(MyTableViewCell.self, forCellReuseIdentifier: "cell")
        table.register(UINib(nibName: "MyTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
    }
     func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        print("deleted...")
    }
}
extension ViewController: UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return modelName.count
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return modelName[section][0]
    }
}
extension ViewController: UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return modelName[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        print(indexPath.row)
        print(indexPath.row)
        cell.textLabel?.text = modelName[indexPath.section][indexPath.row]
        return cell
    }
}

